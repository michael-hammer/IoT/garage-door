#include <Homie.h>
#include <MOD_IO.h>
#include <BounceI2C.h>
#include <Wire.h>

// Flags for door status
#define CLOSED 0
#define OPENED 1
#define RUNNING 2
#define UNDEF 7

// Action bits 
#define OPEN_TOGGLE 0
#define CLOSE_TOGGLE 1
#define STOP_TOGGLE 2

#define TOGGLE_TIME 500

MOD_IO modio(0x58);

HomieNode GarageDoorStatusNode("state", "State", "state");
HomieNode GarageDoorActionNode("action", "Action", "action");

BounceI2C debounceDoorClosed = BounceI2C();
BounceI2C debounceDoorOpened = BounceI2C();
BounceI2C debounceDoorRunning = BounceI2C();

uint8_t i2cStatus = 0x00;
uint8_t doorStatus = _BV(UNDEF);
uint8_t actionStatus = 0x00;

bool doorRunning = false;

unsigned long actionToggleTimer_open = 0;
unsigned long actionToggleTimer_close = 0;
unsigned long actionToggleTimer_stop = 0;

//---------------------------------------------------------------------------------------
void updateStatus(void) {
    uint8_t newDoorStatus;
    if( debounceDoorOpened.update() || debounceDoorClosed.update() || debounceDoorRunning.update() )
    {
        newDoorStatus = i2cStatus;
        Homie.getLogger() << "State changed: " << String(newDoorStatus,BIN) << endl;
    
        /* Why should we be opened and closed at the same time?
        if( ((bool)(newDoorStatus & _BV(OPENED)) != (bool)(newDoorStatus & _BV(CLOSED))) ) {
            newDoorStatus &= ~_BV(UNDEF);
        } else {
            newDoorStatus |= _BV(UNDEF);
        } */
     
        if ( newDoorStatus != doorStatus ) {
            if( (bool)(newDoorStatus & _BV(RUNNING)) ) {
                Homie.getLogger() << "Door is running" << endl;
                doorRunning = true;
                GarageDoorStatusNode.setProperty("state").send("running");
            } else if(newDoorStatus & _BV(OPENED)) {
                Homie.getLogger() << "Door is opened" << endl;
                doorRunning = false;
                GarageDoorStatusNode.setProperty("state").send("opened");
            } else if(newDoorStatus & _BV(CLOSED)) {
                Homie.getLogger() << "Door is closed" << endl;
                doorRunning = false;
                GarageDoorStatusNode.setProperty("state").send("closed");
            } else { // RESTING
                Homie.getLogger() << "Door is resting" << endl;
                doorRunning = false;
                GarageDoorStatusNode.setProperty("state").send("resting");
            }            
            doorStatus = newDoorStatus;
        }
    } 
}

//---------------------------------------------------------------------------------------
void openRelay(void) {
    if( actionStatus & _BV(OPEN_TOGGLE) ) {
        if( millis() - actionToggleTimer_open > TOGGLE_TIME ) {
            modio.setRelay(modio.RELAY1, modio.OFF);
            actionStatus &= ~_BV(OPEN_TOGGLE);
            GarageDoorActionNode.setProperty("open").send("false");
        }
    }

    if( actionStatus & _BV(CLOSE_TOGGLE) ) {
        if( millis() - actionToggleTimer_close > TOGGLE_TIME ) {
            modio.setRelay(modio.RELAY2, modio.OFF);
            actionStatus &= ~_BV(CLOSE_TOGGLE);
            GarageDoorActionNode.setProperty("close").send("false");
        }
    }

    if( actionStatus & _BV(STOP_TOGGLE) ) {
        if( millis() - actionToggleTimer_stop > TOGGLE_TIME ) {
            modio.setRelay(modio.RELAY3, modio.OFF);
            actionStatus &= ~_BV(STOP_TOGGLE);
            GarageDoorActionNode.setProperty("stop").send("false");
        }
    }
}

//---------------------------------------------------------------------------------------
bool closeRelay( uint8_t actionToggle ) {

    // Should we return if actionStatus != 0x00?

    // If the door is running we first stop before we take any other command
    if( doorRunning && (actionToggle != STOP_TOGGLE) )
    {
        Homie.getLogger() << "We are currently running - we should stop ..." << endl;
        actionToggle = STOP_TOGGLE;
    }

    // Stop command on UAP1 behaves like normal door switch - sending STOP if door is not moving would make it move!
    if( !doorRunning && (actionToggle == STOP_TOGGLE) )
        return true;
 
    switch(actionToggle)
    {
        case( OPEN_TOGGLE ):
            Homie.getLogger() << "Send opening action" << endl;
            actionStatus |= _BV(OPEN_TOGGLE);
            modio.setRelay(modio.RELAY1, modio.ON);
            actionToggleTimer_open = millis();
            break;
        case ( CLOSE_TOGGLE ):
            Homie.getLogger() << "Send closing action" << endl;
            actionStatus |= _BV(CLOSE_TOGGLE);
            modio.setRelay(modio.RELAY2, modio.ON);
            actionToggleTimer_close = millis();
            break;
        case ( STOP_TOGGLE ):
            Homie.getLogger() << "Send stopping action" << endl;
            actionStatus |= _BV(STOP_TOGGLE);
            modio.setRelay(modio.RELAY3, modio.ON);
            actionToggleTimer_stop = millis();
            break;
    }
    return true;
}

//---------------------------------------------------------------------------------------
bool garageOpenHandler(const HomieRange& range, const String& value) {
    Homie.getLogger() << "Received payload on open handler" << endl;
    if( value == "true" )
    {
        Homie.getLogger() << "Open the door" << endl;
        return closeRelay( OPEN_TOGGLE );
    } else
        return false;
}

//---------------------------------------------------------------------------------------
bool garageCloseHandler(const HomieRange& range, const String& value) {
    Homie.getLogger() << "Received payload on close handler" << endl;
    if( value == "true" )
    {
        Homie.getLogger() << "Close the door" << endl;
        return closeRelay( CLOSE_TOGGLE );
    } else
        return false;
}

//---------------------------------------------------------------------------------------
bool garageStopHandler(const HomieRange& range, const String& value) {
    Homie.getLogger() << "Received payload on stop handler" << endl;
    if( value == "true" )
    {
        Homie.getLogger() << "Stop the door from moving" << endl;
        return closeRelay( STOP_TOGGLE );
    } else
        return false;
}

//---------------------------------------------------------------------------------------
void loopHandler() {
    i2cStatus = modio.digitalReadAll() & 0x0F;
    updateStatus();
    openRelay();
}

//---------------------------------------------------------------------------------------
void setup() {
    Serial.begin(115200);
    Serial.println();
    Serial.println();

    modio.begin();
    delay(10);

    Homie_setFirmware("garagedoor", "0.3.3");
    Homie.disableLedFeedback();
    Homie.setLoopFunction(loopHandler);

    debounceDoorClosed.attach(&i2cStatus,0);
    debounceDoorClosed.interval(20);
    debounceDoorOpened.attach(&i2cStatus,1);
    debounceDoorOpened.interval(20);
    debounceDoorRunning.attach(&i2cStatus,2);
    debounceDoorRunning.interval(100);

    GarageDoorActionNode.advertise("open").setName("Open").setDatatype("boolean").setRetained(false).settable(garageOpenHandler);
    GarageDoorActionNode.advertise("close").setName("Close").setDatatype("boolean").setRetained(false).settable(garageCloseHandler);
    GarageDoorActionNode.advertise("stop").setName("Stop").setDatatype("boolean").setRetained(false).settable(garageStopHandler);
    GarageDoorStatusNode.advertise("state").setName("State").setDatatype("enum").setFormat("opened,closed,running,resting");

    Homie.getMqttClient().setKeepAlive(45);
    Homie.setup();
}

void loop() {
    Homie.loop();
}