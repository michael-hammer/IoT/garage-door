
#include "Arduino.h"
#include "BounceI2C.h"

#define DEBOUNCED_STATE 0
#define UNSTABLE_STATE  1
#define STATE_CHANGED   3

BounceI2C::BounceI2C()
    : previous_millis(0)
    , interval_millis(10)
    , state(0)
    , value(0)
	, position(0)
{}

bool BounceI2C::readState()
{
  return( ((*(this->value)) >> position) & 0x01 );
}

void BounceI2C::attach(uint8_t* value, uint8_t position) {
    this->value = value;
    this->position = position;
    if ( this->readState() ) {
        state = _BV(DEBOUNCED_STATE) | _BV(UNSTABLE_STATE);
    }
	
	previous_millis = millis();
}

void BounceI2C::interval(uint16_t interval_millis)
{
    this->interval_millis = interval_millis;
}

bool BounceI2C::update()
{
    // Read the state of the switch in a temporary variable.
    bool currentState = this->readState();
    state &= ~_BV(STATE_CHANGED);

    // If the reading is different from last reading, reset the debounce counter
    if ( currentState != (bool)(state & _BV(UNSTABLE_STATE)) ) {
        previous_millis = millis();
        state ^= _BV(UNSTABLE_STATE);
    } else
        if ( millis() - previous_millis >= interval_millis ) {
            // We have passed the threshold time, so the input is now stable
            // If it is different from last state, set the STATE_CHANGED flag
            if ((bool)(state & _BV(DEBOUNCED_STATE)) != currentState) {
                previous_millis = millis();
                state ^= _BV(DEBOUNCED_STATE);
                state |= _BV(STATE_CHANGED);
            }
        }

    return state & _BV(STATE_CHANGED);
}

bool BounceI2C::read()
{
    return ((this->state) & _BV(DEBOUNCED_STATE));
}

bool BounceI2C::rose()
{
    return ((this->state) & _BV(DEBOUNCED_STATE) ) && ( (this->state) & _BV(STATE_CHANGED));
}

bool BounceI2C::fell()
{
    return !((this->state) & _BV(DEBOUNCED_STATE) ) && ( (this->state) & _BV(STATE_CHANGED));
}
